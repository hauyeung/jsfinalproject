window.onload = init;

function init() {

	Modernizr.load([            
            {
                test: Modernizr.geolocation,
                yep : "geolocation.js",
                nope: "noGeolocation.js",				
            },
            {
                test: Modernizr.localstorage,
                yep : "localStorage.js",
                nope: "noLocalStorage.js",
				
            }
        ]);	


}

