navigator.geolocation.getCurrentPosition(getMyLocation, locationError);

var map = null;
function getMyLocation(position) {
    var latitude = position.coords.latitude;
    var longitude = position.coords.longitude;
	if (Modernizr.localstorage){
		localStorage['lat'] = latitude;
		localStorage['lon'] = longitude;
	}
	
	if (!map)	{
		showMap(latitude, longitude);
	}
    addMarker(latitude, longitude);
}      
function locationError(error) {
    var errorTypes = {
        0: "Unknown error",
        1: "Permission denied by user",
        2: "Position not available",
        3: "Request timed out"
    };
    var errorMessage = errorTypes[error.code];
    if (error.code == 0 || error.code == 2) {
        errorMessage += " " + error.message;
    }
    console.log(errorMessage);    
}    

function addMarker(lat, lon) {
    var googleLatLong = new google.maps.LatLng(lat, lon);
    var markerOptions = {
        position: googleLatLong,
        map: map        
    }
    var marker = new google.maps.Marker(markerOptions);
}

function showMap(lat, lon) {
    var googleLatLong = new google.maps.LatLng(lat, lon);
    var mapOptions = {
        zoom: 12,
        center: googleLatLong,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var mapDiv = document.getElementById("map");
    map = new google.maps.Map(mapDiv, mapOptions);
    map.panTo(googleLatLong);
}